// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import Adw from 'gi://Adw';
import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';


import { Collections } from '../services/Collections.js';
import { Database } from '../services/Database.js';
import { DBus } from '../services/DBus.js';
import { Player } from '../services/Player.js';
import { Zaps } from '../services/Zaps.js';

import { Window } from '../widgets/Window.js';


/** @type {?Collections} */
globalThis.collections = null;
/** @type {?Database} */
globalThis.database = null;
/** @type {?Player} */
globalThis.player = null;
/** @type {?Gio.Settings} */
globalThis.settings = null;
/** @type {?Zaps} */
globalThis.zaps = null;


/**
 * The application.
 */
export class Application extends Adw.Application {

    dBus;

    static {
        GObject.registerClass({
            GTypeName: 'ZapApplication',
        }, this);
    }

    /**
     * @param {object} params Parameter object.
     */
    constructor({ ...params } = {}) {
        console.debug('Initializing Application...');
        super({
            application_id: pkg.name,
            flags: Gio.ApplicationFlags.FLAGS_NONE,
            ...params,
        });
        // Translators: Application name, avoid translating it!
        GLib.set_application_name(_('Zap'));
        globalThis.settings = new Gio.Settings({ schemaId: this.applicationId });
        globalThis.player = new Player();
        globalThis.database = new Database();
        globalThis.zaps = new Zaps();
        globalThis.collections = new Collections();
        this.dBus = new DBus();
        console.debug('Application initialized.');
    }

    /**
     * Startup virtual method.
     */
    vfunc_startup() {
        console.debug('Starting Application...');
        super.vfunc_startup();
        globalThis.player.start();
        globalThis.database.start();
        globalThis.zaps.start();
        globalThis.collections.start();
        this.dBus.start();
        this.#setupActions();
        this.#setupAccelerators();
        console.debug('Application started.');
    }

    /**
     * Activate virtual method.
     */
    vfunc_activate() {
        console.debug('Application has been activated.');
        this.newWindow();
    }

    /**
     * Shutdown virtual method.
     */
    vfunc_shutdown() {
        console.debug('Shutting down Application...');
        this.dBus.exit();
        globalThis.player.exit();
        globalThis.collections.exit();
        globalThis.zaps.exit();
        globalThis.database.exit();
        super.vfunc_shutdown();
    }

    /**
     * Open a new window.
     */
    newWindow() {
        console.debug('Opening a new window...');
        const window = new Window({ application: this });
        window.present();
    }

    /**
     * Setup the actions.
     */
    #setupActions() {
        console.debug('Setting up actions...');
        [
            {
                name: 'quit',
                parameterType: null,
                callback: (action, params) => {
                    this.quit();
                },
            },
            {
                name: 'new-window',
                parameterType: null,
                callback: (action, params) => {
                    this.newWindow();
                },
            },
        ].forEach(({ name, parameterType, callback }) => {
            const action = new Gio.SimpleAction({ name, parameterType });
            action.connect('activate', callback);
            this.add_action(action);
        });
        console.debug('Actions set up.');
    }

    /**
     * Setup the accelerators.
     */
    #setupAccelerators() {
        console.debug('Setting up accelerators...');
        this.set_accels_for_action('app.new-window', ['<Control>n']);
        this.set_accels_for_action('app.quit', ['<Control>q']);
        this.set_accels_for_action('win.open-add-zap-popup', ['F2']);
        this.set_accels_for_action('win.open-collections-popover', ['F3']);
        this.set_accels_for_action('window.close', ['<Control>w']);
        console.debug('Accelerators set up.');
    }

}
