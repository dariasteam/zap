Hello,

A new version will soon be released. Planned date: **YYYY-MM-DD**

**Release checklist**

- [ ] Bump version number in `meson.build`
- [ ] Add entry in `CHANGELOG.md`
- [ ] Add release notes to the metainfo file

**Translations**

- [ ] Brazilian Portuguese (@rafaelff)
- [ ] Dutch (@philip.goto)
- [ ] French
- [ ] German (@gastornis)


/label ~release
